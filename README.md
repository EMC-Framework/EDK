The EMC (Easy Minecraft Client) Development Kit
===================

Development kit for [EMC](https://gitlab.com/EMC-Framework/EMC)

You can find API documentation at the [EMC website](https://emc-framework.gitlab.io/)

Setting up your IDE to develop with EMC
-------------------

1. Make a new gradle project and match your build.gradle file with [the example one](https://gitlab.com/EMC-Framework/EDK/blob/master/build.gradle).
2. Add the [mappings.sh](https://gitlab.com/EMC-Framework/EDK/blob/master/mappings.sh) or [mappings.bat](https://gitlab.com/EMC-Framework/EDK/blob/master/mappings.bat) (depending on your OS) to the root of your project and run it.
3. Refresh gradle

Making your first mod with EMC
-------------------

1. Create a file called `client.json` in the resource folder of your project, this file is used so that EMC can find your main class and info about your mod.
2. In that file add the following:

```
{
    "name": "TestMod",
    "website": "https://gitlab.com/EMC-Framework/EDK",
    "author": "Deftware",
    "minVersion": "16.0.0",
    "version": 1,
    "main":"me.deftware.mod.Main",
    "updateLinkOverride": false,
    "scheme": 4
}
```

3. Create the main class you specified in the `client.json` file.
4. Extend your class by `EMCMod`, add the required methods.
5. Here's an example of a main class:

```
package me.deftware.mod;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import me.deftware.client.framework.command.CommandBuilder;
import me.deftware.client.framework.command.CommandRegister;
import me.deftware.client.framework.command.EMCModCommand;
import me.deftware.client.framework.event.EventBus;
import me.deftware.client.framework.event.EventHandler;
import me.deftware.client.framework.event.events.EventUpdate;
import me.deftware.client.framework.wrappers.IChat;
import me.deftware.client.framework.main.EMCMod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main extends EMCMod {

	public static final Logger LOGGER = LogManager.getLogger("TestMod");
	public static Main INSTANCE;

	@Override
	public void initialize() {
		// Set the INSTANCE to this, so you can call Main.INSTANCE to get EMCMod functions
		// for your mod, such as Main.INSTANCE.getSettings()
		INSTANCE = this;
		// Here you initialize anything needed for the mod
		// We need to register this class to handle events
		// You can also create a new class and extend it by EventListener
		// to have EMC automatically register and pass events to it
		EventBus.registerClass(this.getClass(), this);
		// We can also register a custom client command here
		CommandRegister.registerCommand(new ExampleCommand());
	}

	@Override
	public void postInit() {
		// This method is called after Minecraft has been fully initialized
		// Here you can call functions such as IMinecraft.setIGuiSCreen
		LOGGER.info("TestMod active!");
	}

	@EventHandler
	public void onUpdate(EventUpdate event) {
		// Example of an update event, called 20 times a second
	}

	public class ExampleCommand extends EMCModCommand {

		@Override
		public CommandBuilder getCommandBuilder() {
			// For more info on how to use commands, look up the documentation for Mojang Brigadier
			return new CommandBuilder().set((LiteralArgumentBuilder) LiteralArgumentBuilder.literal("test")
					.executes(c -> {
						IChat.sendClientMessage("It works!");
						return 1;
					})
			);
		}

	}

}
```

If you want more help you can check out these official EMC mods:

* [EMC Marketplace](https://gitlab.com/EMC-Framework/EMC-Marketplace-Mod)
* [Optifine Installer](https://gitlab.com/EMC-Framework/optifine)

Installing your mod
-------------------

Once you're done with your mod, run the gradle `build` task. Copy your mod jar file from `build/libs/` to `.minecraft/libraries/EMC/<Minecraft version>/` and start Minecraft with EMC (See below on how to start Minecraft with EMC).

Running EMC in the Minecraft launcher
-------------------

1. Download the [EMC 1.15.1](https://gitlab.com/EMC-Framework/EDK/1.15.1-EMC/) dir and put it in `.minecraft/versions/`
2. Create a new profile and select `release 1.15.1-EMC`
3. Start Minecraft

License
-------------------

EMC is licensed under MIT
